# Zines

Fanzines para colectivizar saberes, abrir discusiones y copiar la cultura. Se encuentran en formato PDF, para lectura digital y para impresión en formato fanzine.

Los fanzines se encuentran en tamaño carta. Se recomienda imprimir sin margen en la hoja.

Estos zines también están disponibles en:
https://colectivodisonancia.net/zines/
https://cloud.disroot.org/s/ezoecDQFdBdwCzy



## Lista de textos
* Aaron Swartz. *[Manifiesto por la Guerrilla del Acceso Abierto](https://gitlab.com/cdisonancia/zines/-/raw/master/Swartz%20Manifiesto%20Guerrilla%20de%20Acceso%20Abierto/Swartz_Online.pdf)*.

* Dmytri Kleiner. *[Los hackers no pueden solucionar la vigilancia](https://gitlab.com/cdisonancia/zines/-/raw/master/Kleiner%20Los%20Hackers%20no%20pueden%20solucionar%20la%20vigilancia/Kleiner_Hackers_Online.pdf)*.

* Douglas Rushkoff. *[La superviviencia del más rico](https://gitlab.com/cdisonancia/zines/-/raw/master/Rushkoff%20La%20superviviencia%20del%20m%C3%A1s%20rico/Rushkoff_online.pdf)*.

* Colectivo Disonancia. *[De armas, cables y otros artefactos](https://gitlab.com/cdisonancia/zines/-/raw/master/Disonancia%20Armas%20Cables%20y%20otros%20Artefactos/Disonancia_Armas%20Cables%20y%20otros%20Artefactos_online.pdf)*.

  


## Licencia

Esta serie de zines están bajo [Licencia de Producción de Pares](https://endefensadelsl.org/ppl_deed_es.html), por lo que **eres libre de descargar, copiar, vender y hacer obras derivadas** bajo las siguientes condiciones:

![Producción de Pares by](https://colectivodisonancia.net/wp-content/uploads/2020/12/by.png) **Atribución**: dar reconocimiento a la autoría y la edición de la obra.

![Producción de Pares SA](https://colectivodisonancia.net/wp-content/uploads/2020/12/sa.png) **Compartir bajo misma licencia**: si se crea una obra derivada de esta, debe tener esta misma licencia.

![Producción de Pares NC](https://colectivodisonancia.net/wp-content/uploads/2020/12/nc.png) **No Capitalista**: este obra solo puede ser  comercializada por organizaciones de trabajadores autogestionados,  cooperativas, organizaciones y colectivos sin fines de lucro en donde no existan relaciones de explotación laboral.

Optamos por esta licencia para **contribuir a la autogestión de organizaciones radicales que quieran vender estas obras y recibir íntegramente el beneficio**, pero evitando que la empresas y organizaciones capitalistas puedan lucrar con ellas.